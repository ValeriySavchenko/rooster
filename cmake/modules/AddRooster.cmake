function(rooster_update_compile_flags name)
  set_property(TARGET ${name} APPEND_STRING PROPERTY
    COMPILE_FLAGS "-fno-rtti")
  set_property(TARGET ${name} PROPERTY CXX_STANDARD 14)
endfunction()

function(add_rooster_executable name)
  add_executable(${name} ${ARGN})
  rooster_update_compile_flags(${name})
endfunction()

function(add_rooster_library name)
  add_library(${name} ${ARGN})
  rooster_update_compile_flags(${name})
endfunction()
